
import { FormGroup, FormControl, Validator, Validators, ValidatorFn, ValidationErrors } from '@angular/forms';

import { Component, DoCheck, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.css'],
  templateUrl:'./app.component.html' 
})
export class AppComponent implements OnInit,DoCheck{
    ngDoCheck(): void {
      console.log(this.registerationFormGroup.value)
    }
  
    registerationFormGroup:FormGroup
    get form(){
      return this.registerationFormGroup.controls
    }
    ngOnInit(){
       this.registerationFormGroup = new FormGroup({
         firstName:new FormControl('',[
           Validators.required,
           Validators.maxLength(32)
         ]),
         lastName:new FormControl('',[
          Validators.required,
          Validators.maxLength(32)
        ]),
         email:new FormControl('',[
          Validators.required,
          Validators.email
        ]),
         areaCode:new FormControl('',[
          Validators.required,
          Validators.pattern(/^-?(0|[1-9]\d*)?$/)
        ]),
         phoneNumber:new FormControl('',[
          Validators.required,
          Validators.pattern(/^-?(0|[1-9]\d*)?$/)
        ]),
         password:new FormGroup({
           pass:new FormControl('',[
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(128),
          ]),
           confirmPass:new FormControl('',[
            Validators.required,
            Validators.minLength(8),
            Validators.maxLength(128)
          ] )
         },{validators:checkConfirmPasswordValidator}),
         address1:new FormControl('',[
          Validators.required,
        ]),
         address2:new FormControl('',[
          Validators.required
        ]),
         city:new FormControl('',[
          Validators.required,
        ]),
         state:new FormControl('',[
          Validators.required,
        ]),
         postal:new FormControl('',[
          Validators.required,
          Validators.pattern(/^-?(0|[1-9]\d*)?$/)
        ]),
         country:new FormControl('',[
          Validators.required
        ]),
         month:new FormControl('',[
          Validators.required
        ]),
         day:new FormControl('',[
          Validators.required
        ]),
         year:new FormControl('',[
          Validators.required
        ]),
        hearby:new FormGroup({
          fri:new FormControl(),
          google:new FormControl(),
          blog:new FormControl(),
          news:new FormControl()
        }),
        membershipRule:new FormControl('',[
          Validators.required
        ]),
        privacyPolicy:new FormControl('',[
          Validators.required
        ])
       })
    }
    onSubmit(){
      alert(JSON.stringify(this.registerationFormGroup.value,null,4))
    }
} 

export const checkConfirmPasswordValidator:ValidatorFn = (control:FormGroup):ValidationErrors | null=>{
  const password = control.get('pass')
  const confirmPassword = control.get('confirmPass')
  return password && confirmPassword && password.value !== confirmPassword.value ? {passwordsNotMatch:true}: null
}

export const checkboxesValidator:ValidatorFn = (control:FormGroup):ValidationErrors | null=>{
  const choice1 = control.get('fri')
  const choice2 = control.get('google')
  const choice3 = control.get('blog')
  const choice4 = control.get('news')
  return choice1.value && choice2.value && choice3.value && choice4.value ? null : {noSelect:true}
}
